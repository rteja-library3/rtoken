package rtoken_test

import (
	"testing"

	"github.com/pquerna/otp"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestOTPDigitsSix(t *testing.T) {
	digit := rtoken.OTPDigitsSix

	uints := digit.Uint()
	otpDigit := digit.OTPDigits()

	assert.Equal(t, uint(6), uints, "[TestOTPDigitsSix] Uint should 6")
	assert.Equal(t, otp.DigitsSix, otpDigit, "[TestOTPDigitsSix] Digit should otp.DigitsSix")
}

func TestOTPDigitsEight(t *testing.T) {
	digit := rtoken.OTPDigitsEight

	uints := digit.Uint()
	otpDigit := digit.OTPDigits()

	assert.Equal(t, uint(8), uints, "[TestOTPDigitsEight] Uint should 8")
	assert.Equal(t, otp.DigitsEight, otpDigit, "[TestOTPDigitsEight] Digit should otp.DigitsEight")
}
