package rtoken

import "time"

type OTPProperty struct {
	Secret     []byte
	Issuers    string
	Duration   time.Duration
	Algorithm  OTPAlgorithm
	Digits     OTPDigits
	SKEW       uint
	SecretSize uint
}

func (o *OTPProperty) SetSecret(secret []byte) *OTPProperty {
	o.Secret = secret
	return o
}

func (o *OTPProperty) SetIssuers(issuers string) *OTPProperty {
	o.Issuers = issuers
	return o
}

func (o *OTPProperty) SetDuration(duration time.Duration) *OTPProperty {
	o.Duration = duration
	return o
}

func (o *OTPProperty) SetAlgorithm(algorithm OTPAlgorithm) *OTPProperty {
	o.Algorithm = algorithm
	return o
}

func (o *OTPProperty) SetDigits(digits OTPDigits) *OTPProperty {
	o.Digits = digits
	return o
}

func (o *OTPProperty) SetSKEW(skew uint) *OTPProperty {
	o.SKEW = skew
	return o
}

func (o *OTPProperty) SetSecretSize(secretSize uint) *OTPProperty {
	o.SecretSize = secretSize
	return o
}

func NewOTPProperty() *OTPProperty {
	return &OTPProperty{}
}

func MergeOTPProperty(props ...*OTPProperty) *OTPProperty {
	p := NewOTPProperty()

	for _, prop := range props {
		if len(prop.Secret) != 0 {
			p.Secret = prop.Secret
		}

		if prop.Issuers != "" {
			p.Issuers = prop.Issuers
		}

		if prop.Duration != 0 {
			p.Duration = prop.Duration
		}

		if prop.Algorithm != 0 {
			p.Algorithm = prop.Algorithm
		}

		if prop.Digits != 0 {
			p.Digits = prop.Digits
		}

		if prop.SKEW != 0 {
			p.SKEW = prop.SKEW
		}

		if prop.SecretSize != 0 {
			p.SecretSize = prop.SecretSize
		}
	}

	if p.Duration == 0 {
		p.Duration = time.Second * 30
	}

	if p.Algorithm == 0 {
		p.Algorithm = OTPAlgorithmSHA512
	}

	if p.Digits != 6 && p.Digits != 8 {
		p.Digits = OTPDigitsSix
	}

	if p.SecretSize == 0 {
		p.SecretSize = 20
	}

	return p
}
