package rtoken

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/pquerna/otp"
)

type OTPAlgorithm uint

func (o OTPAlgorithm) Uint() uint {
	return uint(o)
}

func (o OTPAlgorithm) OTPAlgorithm() otp.Algorithm {
	switch o {
	case OTPAlgorithmSHA1:
		return otp.AlgorithmSHA1
	case OTPAlgorithmSHA256:
		return otp.AlgorithmSHA256
	case OTPAlgorithmMD5:
		return otp.AlgorithmMD5
	default:
		return otp.AlgorithmSHA512
	}
}

const (
	OTPAlgorithmSHA1   OTPAlgorithm = 1
	OTPAlgorithmSHA256 OTPAlgorithm = 2
	OTPAlgorithmSHA512 OTPAlgorithm = 3
	OTPAlgorithmMD5    OTPAlgorithm = 4
)

type OTPDigits uint

func (o OTPDigits) Uint() uint {
	return uint(o)
}

func (o OTPDigits) OTPDigits() otp.Digits {
	if o == OTPDigitsSix {
		return otp.DigitsSix
	}

	return otp.DigitsEight
}

const (
	OTPDigitsSix   OTPDigits = 6
	OTPDigitsEight OTPDigits = 8
)

type JWTSigningMethod uint

func (j JWTSigningMethod) Uint() uint {
	return uint(j)
}

func (j JWTSigningMethod) SigningMethodHMAC() *jwt.SigningMethodHMAC {
	switch j {
	case JWTSigningMethodHS256:
		return jwt.SigningMethodHS256
	case JWTSigningMethodHS384:
		return jwt.SigningMethodHS384
	}

	return jwt.SigningMethodHS512
}

const (
	JWTSigningMethodHS256 JWTSigningMethod = 1
	JWTSigningMethodHS384 JWTSigningMethod = 2
	JWTSigningMethodHS512 JWTSigningMethod = 3
)
