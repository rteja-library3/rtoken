package rtoken

import (
	"context"
	"time"

	"github.com/go-chi/jwtauth"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/sirupsen/logrus"
)

type jwtLestrrat struct {
	logger   *logrus.Logger
	aud      string
	iss      string
	duration time.Duration
	auth     *jwtauth.JWTAuth
}

func NewJWTLestrrat(auth *jwtauth.JWTAuth, logger *logrus.Logger, props ...*JWTProperty) Token {
	prop := MergeJWTProperty(props...)

	return &jwtLestrrat{
		logger:   logger,
		aud:      prop.Audience,
		iss:      prop.Issuers,
		duration: prop.ExpiredDuration,
		auth:     auth,
	}
}

func (j jwtLestrrat) Generate(ctx context.Context, claimData map[string]interface{}) (tokenString string, err error) {
	tmNow := time.Now().UTC()

	claimData["aud"] = j.aud
	claimData["iss"] = j.iss
	claimData["iat"] = tmNow.Unix()
	claimData["exp"] = tmNow.Add(j.duration).Unix()

	_, tokenString, err = j.auth.Encode(claimData)
	if err != nil {
		j.logger.
			WithFields(logrus.Fields{
				"ACTION":    "auth.Encode",
				"ClaimData": claimData,
			}).
			Error(err)

		return
	}

	return
}

func (j jwtLestrrat) Parse(ctx context.Context, tokenString string) (claimdata map[string]interface{}, err error) {
	var token jwt.Token

	token, err = j.auth.Decode(tokenString)
	if err != nil {
		j.logger.
			WithFields(logrus.Fields{
				"ACTION":      "auth.Decode",
				"TokenString": tokenString,
			}).
			Error(err)

		return
	}

	claimdata, err = token.AsMap(ctx)
	return
}
