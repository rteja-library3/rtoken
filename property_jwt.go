package rtoken

import "time"

type JWTProperty struct {
	Secret          []byte
	ExpiredDuration time.Duration
	Audience        string
	Issuers         string
	SigningMethod   JWTSigningMethod
}

func (j *JWTProperty) SetSecret(secret []byte) *JWTProperty {
	j.Secret = secret
	return j
}

func (j *JWTProperty) SetExpiredDuration(duration time.Duration) *JWTProperty {
	j.ExpiredDuration = duration
	return j
}

func (j *JWTProperty) SetAudience(audience string) *JWTProperty {
	j.Audience = audience
	return j
}

func (j *JWTProperty) SetIssuers(issuers string) *JWTProperty {
	j.Issuers = issuers
	return j
}

func (j *JWTProperty) SetSigningMethod(signingMethod JWTSigningMethod) *JWTProperty {
	j.SigningMethod = signingMethod
	return j
}

func NewJWTProperty() *JWTProperty {
	return &JWTProperty{}
}

func MergeJWTProperty(props ...*JWTProperty) *JWTProperty {
	p := NewJWTProperty()

	for _, prop := range props {
		if len(prop.Secret) > 0 {
			p.Secret = prop.Secret
		}

		if prop.ExpiredDuration > 0 {
			p.ExpiredDuration = prop.ExpiredDuration
		}

		if prop.Audience != "" {
			p.Audience = prop.Audience
		}

		if prop.Issuers != "" {
			p.Issuers = prop.Issuers
		}

		if prop.SigningMethod > 0 {
			p.SigningMethod = prop.SigningMethod
		}
	}

	if p.ExpiredDuration == 0 {
		p.ExpiredDuration = time.Minute * 15
	}

	if p.SigningMethod == 0 {
		p.SigningMethod = JWTSigningMethodHS512
	}

	return p
}
