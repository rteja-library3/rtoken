package rtoken_test

import (
	"context"
	"math"
	"testing"

	"github.com/go-chi/jwtauth"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestJWTLestrratGenerateSuccess(t *testing.T) {
	// token auth
	tokenAuth := jwtauth.New("HS256", []byte("test"), nil)

	token := rtoken.NewJWTLestrrat(
		tokenAuth,
		logrus.New(),
		rtoken.
			NewJWTProperty().
			SetSecret([]byte("test")).
			SetSigningMethod(rtoken.JWTSigningMethodHS256),
	)

	claim := map[string]interface{}{}

	tokenString, err := token.Generate(context.Background(), claim)

	assert.NoError(t, err, "[TestJWTLestrratGenerateSuccess] Should not error")
	assert.NotEmpty(t, tokenString, "[TestJWTLestrratGenerateSuccess] Token should not empty")
}

func TestJWTLestrratGenerateError(t *testing.T) {
	// token auth
	tokenAuth := jwtauth.New("HS256", []byte("test"), nil)

	token := rtoken.NewJWTLestrrat(
		tokenAuth,
		logrus.New(),
		rtoken.
			NewJWTProperty().
			SetSecret([]byte("test")).
			SetSigningMethod(rtoken.JWTSigningMethodHS256),
	)

	claim := map[string]interface{}{
		"x": math.Inf(1),
	}

	tokenString, err := token.Generate(context.Background(), claim)

	assert.Error(t, err, "[TestJWTLestrratGenerateError] Should error")
	assert.Empty(t, tokenString, "[TestJWTLestrratGenerateError] Token should empty")
}

func TestJWTLestrratParseSuccess(t *testing.T) {
	// token auth
	tokenAuth := jwtauth.New("HS256", []byte("test"), nil)

	token := rtoken.NewJWTLestrrat(
		tokenAuth,
		logrus.New(),
		rtoken.
			NewJWTProperty().
			SetSecret([]byte("test")).
			SetSigningMethod(rtoken.JWTSigningMethodHS256),
	)

	cl := map[string]interface{}{}

	tokenString, _ := token.Generate(context.Background(), cl)
	// tokenString := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiIl0sImV4cCI6MTY0NTQ1OTU4NiwiaWF0IjoxNjQ1NDU4Njg2LCJpc3MiOiIifQ.twg8zXzqTCFs421xD3_atcAGz4M2XgWME982EL-nZas"

	claim, err := token.Parse(context.Background(), tokenString)

	assert.NoError(t, err, "[TestJWTLestrratParseSuccess] Should not error")
	assert.NotEmpty(t, claim, "[TestJWTLestrratParseSuccess] Token should not empty")
}

func TestJWTLestrratParseErrorDecode(t *testing.T) {
	// token auth
	tokenAuth := jwtauth.New("HS256", []byte("test"), nil)

	token := rtoken.NewJWTLestrrat(
		tokenAuth,
		logrus.New(),
		rtoken.
			NewJWTProperty().
			SetSecret([]byte("test")).
			SetSigningMethod(rtoken.JWTSigningMethodHS256),
	)

	tokenString := ""

	claim, err := token.Parse(context.Background(), tokenString)

	assert.Error(t, err, "[TestJWTLestrratParseSuccess] Should error")
	assert.Empty(t, claim, "[TestJWTLestrratParseSuccess] Token should empty")
}

func TestJWTLestrratParseErrorTokenAsMap(t *testing.T) {
	// token auth
	tokenAuth := jwtauth.New("HS256", []byte("test"), nil)

	token := rtoken.NewJWTLestrrat(
		tokenAuth,
		logrus.New(),
		rtoken.
			NewJWTProperty().
			SetSecret([]byte("test")).
			SetSigningMethod(rtoken.JWTSigningMethodHS256),
	)

	tokenString := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdxxxWQiOlsiIl0sImV4cCI6MTY0NTQ1OTU4NiwiaWF0IjoxNjQ1NDU4Njg2LCJpc3MiOiIifQ.twg8zXzqTCFs421xD3_atcAGz4M2XgWME982EL-nZxx"

	claim, err := token.Parse(context.Background(), tokenString)

	assert.Error(t, err, "[TestJWTLestrratParseSuccess] Should error")
	assert.Empty(t, claim, "[TestJWTLestrratParseSuccess] Token should empty")
}
