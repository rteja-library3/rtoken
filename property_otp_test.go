package rtoken_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestOTPPropertyWithValue(t *testing.T) {
	prop := rtoken.NewOTPProperty().
		SetSecret([]byte("tes")).
		SetIssuers("iss").
		SetDuration(time.Second * 15).
		SetAlgorithm(rtoken.OTPAlgorithmMD5).
		SetDigits(rtoken.OTPDigitsSix).
		SetSKEW(1).
		SetSecretSize(20)

	mergeProp := rtoken.MergeOTPProperty(prop)

	assert.NotNil(t, prop, "[TestOTPPropertyWithValue] Should not nil")
	assert.NotNil(t, mergeProp, "[TestOTPPropertyWithValue] Should not nil")
}

func TestOTPPropertyWithDefault(t *testing.T) {
	prop := rtoken.NewOTPProperty().
		SetSecret([]byte("tes")).
		SetIssuers("iss").
		SetDigits(7).
		SetSKEW(1)

	mergeProp := rtoken.MergeOTPProperty(prop)

	assert.NotNil(t, prop, "[TestOTPPropertyWithDefault] Should not nil")
	assert.NotNil(t, mergeProp, "[TestOTPPropertyWithDefault] Should not nil")
}
