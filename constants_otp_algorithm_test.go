package rtoken_test

import (
	"testing"

	"github.com/pquerna/otp"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestOTPAlgorithmSHA1(t *testing.T) {
	algo := rtoken.OTPAlgorithmSHA1

	uints := algo.Uint()
	otpalgo := algo.OTPAlgorithm()

	assert.Equal(t, uint(1), uints, "[TestOTPAlgorithmSHA1] Uint should 1")
	assert.Equal(t, otp.AlgorithmSHA1, otpalgo, "[TestOTPAlgorithmSHA1] Algorithm should otp.AlgorithmSHA1")
}

func TestOTPAlgorithmSHA256(t *testing.T) {
	algo := rtoken.OTPAlgorithmSHA256

	uints := algo.Uint()
	otpalgo := algo.OTPAlgorithm()

	assert.Equal(t, uint(2), uints, "[TestOTPAlgorithmSHA256] Uint should 2")
	assert.Equal(t, otp.AlgorithmSHA256, otpalgo, "[TestOTPAlgorithmSHA256] Algorithm should otp.AlgorithmSHA256")
}

func TestOTPAlgorithmSHA512(t *testing.T) {
	algo := rtoken.OTPAlgorithmSHA512

	uints := algo.Uint()
	otpalgo := algo.OTPAlgorithm()

	assert.Equal(t, uint(3), uints, "[TestOTPAlgorithmSHA512] Uint should 3")
	assert.Equal(t, otp.AlgorithmSHA512, otpalgo, "[TestOTPAlgorithmSHA512] Algorithm should otp.AlgorithmSHA512")
}

func TestOTPAlgorithmMD5(t *testing.T) {
	algo := rtoken.OTPAlgorithmMD5

	uints := algo.Uint()
	otpalgo := algo.OTPAlgorithm()

	assert.Equal(t, uint(4), uints, "[TestOTPAlgorithmMD5] Uint should 4")
	assert.Equal(t, otp.AlgorithmMD5, otpalgo, "[TestOTPAlgorithmMD5] Algorithm should otp.AlgorithmMD5")
}
