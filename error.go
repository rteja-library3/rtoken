package rtoken

import "errors"

var (
	ErrClaimNotFound    error = errors.New("claim not found")
	ErrSignatureInvalid error = errors.New("signature is invalid")
)
