package rtoken_test

import (
	"context"
	"math"
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestGenerateSuccess(t *testing.T) {
	header := map[string]interface{}{
		"alg": jwt.SigningMethodHS512.Name,
	}

	token := rtoken.NewJWT(
		logrus.New(),
		header,
		rtoken.
			NewJWTProperty().
			SetSecret([]byte("test")).
			SetSigningMethod(rtoken.JWTSigningMethodHS512),
	)

	claim := map[string]interface{}{
		"email": "aa@aa.com",
	}

	tokenString, err := token.Generate(context.Background(), claim)

	assert.NoError(t, err, "[TestGenerateSuccess] Should not error")
	assert.NotEmpty(t, tokenString, "[TestGenerateSuccess] Token should not empty")
}

func TestGenerateError(t *testing.T) {
	header := map[string]interface{}{
		"tes": "",
		"x":   math.Inf(1),
	}

	token := rtoken.NewJWT(
		logrus.New(),
		header,
		rtoken.NewJWTProperty(),
	)

	claim := map[string]interface{}{
		"email": "aa@aa.com",
	}

	tokenString, err := token.Generate(context.Background(), claim)

	assert.Error(t, err, "[TestGenerateSuccess] Should error")
	assert.Empty(t, tokenString, "[TestGenerateSuccess] Token should empty")
}

func TestParseSuccess(t *testing.T) {
	header := map[string]interface{}{
		"alg": jwt.SigningMethodHS512.Name,
	}

	token := rtoken.NewJWT(
		logrus.New(),
		header,
		rtoken.
			NewJWTProperty().
			SetSecret([]byte("test")).
			SetSigningMethod(rtoken.JWTSigningMethodHS512),
	)

	claim := map[string]interface{}{}

	tokenString, _ := token.Generate(context.Background(), claim)

	cl, err := token.Parse(context.Background(), tokenString)

	assert.NoError(t, err, "[TestParseSuccess] Should not error")
	assert.NotEmpty(t, cl, "[TestParseSuccess] Token should not empty")
}

func TestParseError(t *testing.T) {
	header := map[string]interface{}{}

	token := rtoken.NewJWT(
		logrus.New(),
		header,
		rtoken.NewJWTProperty(),
	)

	tokenString := "e30.eyJhdWQiOiIiLCJlbWFpbCI6ImFhQGFhLmNvbSIsImV4cCI6MTY0NTQ1Nzk0OSwiaWF0IjoxNjQ1NDU3MDQ5LCJpc3MiOiIifQ.DtCFnk_ZL7AyHjfJOwB5F3jcGMFJD8W4-pgjVor-QhU"

	cl, err := token.Parse(context.Background(), tokenString)

	assert.Error(t, err, "[TestParseError] Should error")
	assert.Nil(t, cl, "[TestParseError] Claim should nil")
}
