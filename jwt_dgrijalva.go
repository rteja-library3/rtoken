package rtoken

import (
	"context"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
)

type jwtToken struct {
	logger          *logrus.Logger
	secret          []byte
	expiredDuration time.Duration
	audience        string
	issuers         string
	signingMethod   *jwt.SigningMethodHMAC
	header          map[string]interface{}
}

func NewJWT(logger *logrus.Logger, header map[string]interface{}, props ...*JWTProperty) Token {
	prop := MergeJWTProperty(props...)

	return &jwtToken{
		logger:          logger,
		secret:          prop.Secret,
		expiredDuration: prop.ExpiredDuration,
		audience:        prop.Audience,
		issuers:         prop.Issuers,
		signingMethod:   prop.SigningMethod.SigningMethodHMAC(),
		header:          header,
	}
}

func (j jwtToken) Generate(ctx context.Context, claimdata map[string]interface{}) (tokenString string, err error) {
	token := jwt.New(j.signingMethod)

	// set default with config
	claimdata["aud"] = j.audience
	claimdata["iss"] = j.issuers
	claimdata["iat"] = time.Now().Unix()
	claimdata["exp"] = time.Now().Add(j.expiredDuration).Unix()

	/* Create a map to store our claims */
	claims := token.Claims.(jwt.MapClaims)
	for k, v := range claimdata {
		claims[k] = v
	}

	token.Header = j.header

	tokenString, err = token.SignedString(j.secret)
	if err != nil {
		j.logger.
			WithFields(logrus.Fields{
				"ACTION":    "token.SignedString",
				"Claimdata": claimdata,
			}).
			Error(err)

		return
	}

	return
}

func (j jwtToken) Parse(ctx context.Context, tokenString string) (claimdata map[string]interface{}, err error) {
	token, err := jwt.
		Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
			return j.secret, nil
		})

	if err != nil {
		err = ErrSignatureInvalid
		return
	}

	claimdata, _ = token.Claims.(jwt.MapClaims)
	return
}
