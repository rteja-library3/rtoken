package rtoken_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestJWTPropertyWithValue(t *testing.T) {
	prop := rtoken.NewJWTProperty().
		SetSecret([]byte("tes")).
		SetExpiredDuration(time.Second * 15).
		SetAudience("aud").
		SetIssuers("iss").
		SetSigningMethod(rtoken.JWTSigningMethodHS256)

	mergeProp := rtoken.MergeJWTProperty(prop)

	assert.NotNil(t, prop, "[TestJWTPropertyWithValue] Should not nil")
	assert.NotNil(t, mergeProp, "[TestJWTPropertyWithValue] Should not nil")
}

func TestJWTPropertyWithDefault(t *testing.T) {
	prop := rtoken.NewJWTProperty().
		SetSecret([]byte("tes")).
		SetAudience("aud").
		SetIssuers("iss")

	mergeProp := rtoken.MergeJWTProperty(prop)

	assert.NotNil(t, prop, "[TestJWTPropertyWithDefault] Should not nil")
	assert.NotNil(t, mergeProp, "[TestJWTPropertyWithDefault] Should not nil")
}
