package rtoken_test

import (
	"context"
	"testing"
	"time"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestOTPPQuernaGenerateSuccess(t *testing.T) {
	prop := rtoken.NewOTPProperty().
		SetSecret([]byte("alon-corp-otp-secret")).
		SetIssuers("iss").
		SetDuration(time.Second * 15).
		SetAlgorithm(rtoken.OTPAlgorithmMD5).
		SetDigits(rtoken.OTPDigitsSix).
		SetSKEW(1).
		SetSecretSize(20)

	otp := rtoken.NewOTPToken(logrus.New(), prop)

	claim := map[string]interface{}{
		"email": "aa@aa.com",
	}

	token, err := otp.Generate(context.Background(), claim)

	assert.NoError(t, err, "[TestOTPPQuernaGenerateSuccess] Should not error")
	assert.NotEmpty(t, token, "[TestOTPPQuernaGenerateSuccess] Should not empty")
}

func TestOTPPQuernaGenerateTOTPGenerateError(t *testing.T) {
	prop := rtoken.NewOTPProperty().
		SetSecret([]byte("alon-corp-otp-secret")).
		SetIssuers("iss").
		SetDuration(time.Second * 15).
		SetAlgorithm(rtoken.OTPAlgorithmMD5).
		SetDigits(rtoken.OTPDigitsSix).
		SetSKEW(1).
		SetSecretSize(20)

	otp := rtoken.NewOTPToken(logrus.New(), prop)

	claim := map[string]interface{}{}

	token, err := otp.Generate(context.Background(), claim)

	assert.Error(t, err, "[TestOTPPQuernaGenerateTOTPGenerateError] Should error")
	assert.Empty(t, token, "[TestOTPPQuernaGenerateTOTPGenerateError] Should empty")
}
