package rtoken

import (
	"context"
	"encoding/base32"
	"time"

	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/sirupsen/logrus"
)

type otpTokenPquerna struct {
	logger     *logrus.Logger
	secret     []byte
	issuers    string
	period     uint
	algorithm  otp.Algorithm
	digits     otp.Digits
	skew       uint
	secretSize uint
}

func NewOTPToken(logger *logrus.Logger, props ...*OTPProperty) TokenGenerateAble {
	prop := MergeOTPProperty(props...)

	return &otpTokenPquerna{
		logger:     logger,
		secret:     prop.Secret,
		issuers:    prop.Issuers,
		period:     uint(prop.Duration / time.Second),
		algorithm:  prop.Algorithm.OTPAlgorithm(),
		digits:     prop.Digits.OTPDigits(),
		skew:       prop.SKEW,
		secretSize: prop.SecretSize,
	}
}

func (o otpTokenPquerna) Generate(ctx context.Context, claimdata map[string]interface{}) (tokenString string, err error) {
	var key *otp.Key
	var accountName string

	for _, data := range claimdata {
		if v, ok := data.(string); ok {
			accountName += v
		}
	}

	genOTP := totp.GenerateOpts{
		Issuer:      o.issuers,
		AccountName: accountName,
		Period:      o.period,
		SecretSize:  o.secretSize,
		Secret:      o.secret,
		Digits:      o.digits,
		Algorithm:   o.algorithm,
	}

	key, err = totp.Generate(genOTP)
	if err != nil {
		o.logger.
			WithFields(logrus.Fields{
				"ACTION":      "totp.Generate",
				"AccountName": accountName,
			}).
			Error(err)

		return
	}

	secret := base32.StdEncoding.EncodeToString([]byte(key.String()))
	tokenString, _ = totp.GenerateCodeCustom(
		secret,
		time.Now().UTC(),
		totp.ValidateOpts{
			Period:    o.period,
			Algorithm: o.algorithm,
			Skew:      o.skew,
			Digits:    o.digits,
		},
	)

	return
}
