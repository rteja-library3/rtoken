package rtoken_test

import (
	"testing"

	"github.com/dgrijalva/jwt-go"
	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rtoken"
)

func TestJWTSigningMethodHS256(t *testing.T) {
	digit := rtoken.JWTSigningMethodHS256

	uints := digit.Uint()
	hmac := digit.SigningMethodHMAC()

	assert.Equal(t, uint(1), uints, "[TestJWTSigningMethodHS256] Uint should 1")
	assert.Equal(t, jwt.SigningMethodHS256, hmac, "[TestJWTSigningMethodHS256] Algorithm should jwt.SigningMethodHS256")
}

func TestJWTSigningMethodHS384(t *testing.T) {
	digit := rtoken.JWTSigningMethodHS384

	uints := digit.Uint()
	hmac := digit.SigningMethodHMAC()

	assert.Equal(t, uint(2), uints, "[TestJWTSigningMethodHS384] Uint should 2")
	assert.Equal(t, jwt.SigningMethodHS384, hmac, "[TestJWTSigningMethodHS384] Algorithm should jwt.SigningMethodHS384")
}

func TestJWTSigningMethodHS512(t *testing.T) {
	digit := rtoken.JWTSigningMethodHS512

	uints := digit.Uint()
	hmac := digit.SigningMethodHMAC()

	assert.Equal(t, uint(3), uints, "[TestJWTSigningMethodHS512] Uint should 3")
	assert.Equal(t, jwt.SigningMethodHS512, hmac, "[TestJWTSigningMethodHS512] Algorithm should jwt.SigningMethodHS512")
}
