package rtoken

import "context"

type TokenGenerateAble interface {
	Generate(ctx context.Context, claimdata map[string]interface{}) (tokenString string, err error)
}

type TokenParseAble interface {
	Parse(ctx context.Context, tokenString string) (claimdata map[string]interface{}, err error)
}

type Token interface {
	TokenGenerateAble
	TokenParseAble
}
